function nombre(){
    var a = document.getElementById("name").value;
    if (a=="" || a==" "){
        document.getElementById("message_name").innerHTML="El campo nombre esta vacio";
    }
    else {
        document.getElementById("message_name").innerHTML="";
    }
    deshabilitar();
}
function apellido(){
    var a = document.getElementById("apellid").value;
    if (a=="" || a==" "){
        document.getElementById("message_apellido").innerHTML="El campo apellido esta vacio";
    }
    else {
        document.getElementById("message_apellido").innerHTML="";
    }
    deshabilitar();
}
function edades(){
    var a= document.getElementById("edad").value;
    if (a<18){
        document.getElementById("message_edad").innerHTML="Eres menor de edad";
        document.getElementById("enviar").disabled=true;

    }
    else {
        document.getElementById("message_edad").innerHTML="";
        document.getElementById("enviar").disabled=false;
    }
}
function gmail(){
    var a=document.getElementById("correo").value;
    var b=document.getElementById("correo2").value;
    if (a=="" || a==" " || b=="" || b==" "){
        document.getElementById("message_correo2").innerHTML="Uno de los campos Email esta vacio";
    }
    else{
        document.getElementById("message_correo2").innerHTML="";
    }
    if (a!=b){

        document.getElementById("message_email2").innerHTML="Los campos EMAIL no coinciden";
    }
    else{
        document.getElementById("message_email2").innerHTML="";
    }
    deshabilitar();
}
function deshabilitar() {
    if (document.getElementById("name").value != "" && document.getElementById("apellid").value != ""
        && document.getElementById("correo").value != "" && document.getElementById("correo2").value != ""
        && document.getElementById("edad").value != "" && document.getElementById("dni").value != ""
        && document.getElementById("juego").value != "" && document.getElementById("code").value != ""
    && document.getElementById("card").value!="") {
        document.getElementById("enviar").disabled = false;
    }
    else {
        document.getElementById("enviar").disabled = true;
    }
}

    function DNII() {
        var a = document.getElementById("dni").value;
        if (a == "" || a == " ") {
            document.getElementById("dni").innerHTML = "El campo DNI esta vacio";
        }
        else {
            document.getElementById("dni").innerHTML = "";
        }
        deshabilitar();
    }

    function game() {
        var a = document.getElementById("juego").value;
        if (a == "" || a == " ") {
            document.getElementById("message_game").innerHTML = "El campo juego esta vacio";
        }
        else {
            document.getElementById("message_game").innerHTML = "";
        }
        deshabilitar();
    }
    function codgame() {
        var a=document.getElementById("code").value;
        if (a=="" || a==" "){
            document.getElementById("message_code").innerHTML="El campo codigo del juego esta vacio";
        }
        else{
            document.getElementById("message_code").innerHTML="";
        }
        deshabilitar();
    }
    function creditcard(){
        var a=document.getElementById("card").value;
        if (a=="" || a==" "){
            document.getElementById("message_card").innerHTML="El campo targeta de crédito esta vacio";
        }
        else{
            document.getElementById("message_card").innerHTML="";
        }
        deshabilitar();

}

function password(){
    var a=document.getElementById("password").value;
    var b=document.getElementById("password23").value;

    if(a!=b){
        document.getElementById("message_pass").innerHTML="Las contraseñas son distintas";
    }
}

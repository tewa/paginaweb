var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors= require('cors');


var index = require('./routes/index');
// var users = require('./routes/users');


var dbUrl= "mongodb://dbuser:dbuser@ds147459.mlab.com:47459/juegos?authMechanism=SCRAM-SHA-1";

var db = require ('monk')(dbUrl);

const juegos = db.get('juegos');
const users=db.get('users');

var passport =require ('passport');
var JwtStrategy = require ('passport-jwt').Strategy;
var ExtractJwt = require ('passport-jwt').ExtractJwt;
var jwt =require ('jsonwebtoken');

var opts ={};
opts.jwtFromRequest=ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey="ARROQUITAN";


passport.use(new JwtStrategy(opts,function(jwt_payload,done){
    users.findOne({"_id": jwt_payload._id})
        .then(function(user){
            if (user){
                done (null,user);
            } else{
                done (null,false);
            }
        }).catch(function(error){
            console.log("ERROR " + error);
    });
}));




const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';
const someOtherPlaintextPassword = 'not_bacon';

app.use(cors());

app.post('/api/auth/register', function (req, res) {
    var data = req.body;
    if (!data.username || !data.password) {
        res.json({success: false, msg: 'Please enter username and password.'});
    } else {
        if (data.password === data.password23 ) {
            if (data.password.length <= 8){
                bcrypt.hash(data.password,saltRounds).then(function (hash) {
                    data.password = hash;
                }).then(promise => {
                    users.findOne({username: data.username})
                    .then(function (user) {
                        if (!user) {
                            users.insert(data);
                            return res.json({success: true, msg: 'User created!'});
                        } else {
                            return res.json({success: false, msg: 'Username already exists.'});
                        }
                    });
            });
            }else {
                res.json({success: false, msg: 'Password must contain at least 8 characters.'});
            }
        }else {
            res.json({success: false, msg: 'Password and confirmation password do not match.'});
        }
    }
});




app.post('/api/auth/login', function (req, res) {
    var data = req.body;

    users.findOne({username: data.username})
        .then(function (user) {
            if (!user) {
                res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
            } else {
                bcrypt.compare(data.password, user.password).then(function (response) {
                    if (response) {
                        var token = jwt.sign(
                            {
                                "_id": user._id,
                                "username": user.username
                            },
                            opts.secretOrKey
                        );
                        res.json({success: true, token: 'JWT ' + token});
                    } else {
                        res.status(401).send({success: false, msg: 'Authentication failed. Wrong Password.'});
                    }
                });
            }
        })
});

app.post('/api/auth/logout', function(req,res){
    var token=null;
});

app.get('/api/juegos', function(req,res){
  juegos.find({})
      .then(function(data){
          console.log(data);
    res.json({juegos:data});
  }).catch(function(error){
      console.log(error);
  });
});
app.get('/api/juegos/:id', function(req,res){
     var _id=req.params.id;
    juegos.find({"_id": _id})
        .then(function (data) {
        res.json(data);
    }).catch(function(error){
        console.log(error);
    });
});

app.post('/api/juegos/:id', function(req,res){
    var _id=req.params.id;

    var data= req.body;
    juegos.update({"_id": _id},data)
        .then(function (data) {
            res.json(data);
        }).catch(function(error){
        console.log(error);
    });
});

app.post('/api/juegos', function(req,res){

    var data= req.body;

    juegos.insert(data)
        .then(function (data) {
            res.json({juegos : data});
        }).catch(function(error){
        console.log(error);
    });
});


app.delete('/api/juegos/:id',passport.authenticate('jwt', {session :false}), function(req,res){
   if (req.user){
       console.log(req.user.username);
       var _id =req.params.id;
       console.log(_id);
       juegos.remove({_id:_id})
           .then(function(data){
               console.log("Entra");
               res.json(data);
           }).catch(function(error){
           console.log("ERROR " + error)
       });


   }
   else{
       return res.status(403).send({success : false, msg : 'Unauthorized.'});
   }
});




// app.use('/', index);
//  app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
